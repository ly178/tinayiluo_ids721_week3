# Weekly Mini-Project

[![pipeline status](https://gitlab.com/ly178/tinayiluo_ids721_week3/badges/master/pipeline.svg)](https://gitlab.com/ly178/tinayiluo_ids721_week3/-/commits/master)

## Goals

Create an infrastructure as code for an S3 Bucket using CDK with AWS CodeWhisperer and add bucket properties like versioning and encryption. 

## Overview

This project demonstrates how to create an AWS S3 bucket with versioning and encryption enabled using AWS Cloud Development Kit (CDK) and AWS CodeWhisperer. The project follows a workflow that includes setting up a development environment, configuring AWS CLI, initializing a CDK project, and deploying the S3 bucket to AWS.

## Delivery

### Correct CDK bucket creation

![Screen_Shot_2024-02-10_at_5.11.15_PM](/uploads/1d78cb51d89935ee86a03fc56c7a0b1a/Screen_Shot_2024-02-10_at_5.11.15_PM.png)

### Bucket properties (Versioning and Encryption)

![Screen_Shot_2024-02-10_at_5.26.11_PM](/uploads/dae26d28b1c288dbbbb49120097b1dc0/Screen_Shot_2024-02-10_at_5.26.11_PM.png)

![Screen_Shot_2024-02-10_at_5.26.58_PM](/uploads/51859eb0a0d10cdad181ac86b1c2ce26/Screen_Shot_2024-02-10_at_5.26.58_PM.png)

![Screen_Shot_2024-02-10_at_5.38.45_PM](/uploads/f4126045a8a05b27b563b5e0e2a70211/Screen_Shot_2024-02-10_at_5.38.45_PM.png)

### Documentation

#### Prerequisites

Before starting, ensure you have the following prerequisites:

- Access to CodeCatalyst (a free tier is sufficient).
- A builder ID connected for AWS CodeWhisperer and CodeCatalyst usage.

#### Setup and Configuration

1. **Create a Development Environment:**

   - Create a new directory for the project and initialize it:
     ```bash
     mkdir s3
     npm run build
     ```

2. **AWS IAM Configuration:**

   - Navigate to IAM in the AWS Console and create a new user.
   - Attach `IAMFullAccess` and `AmazonS3FullAccess` policies.
   - Add inline policies for CloudFormation and Systems Manager, granting access to all commands.
   - Create an access key and secret access key, then store them securely.

3. **AWS CLI Configuration:**

   - In your development environment, configure AWS CLI:
     ```bash
     aws configure
     ```
   - Enter the AWS access key and secret access key when prompted. Optionally, specify the region, but note that specifying a region might cause issues later.

4. **CDK Project Initialization:**

   - Create a project directory, navigate into it, and initialize a CDK app:
     ```bash
     mkdir my-cdk-project
     cd my-cdk-project
     cdk init app --language=typescript
     ```

#### Project Development

1. **Modify the CDK Template:**

   - Update the `s3.ts` and `s3-stack.ts` files to define your S3 bucket with desired properties such as versioning and encryption.

2. **Build and Synthesize the CDK Project:**

   - Ensure the project builds correctly:
     ```bash
     npm run build
     ```
   - Create the CloudFormation template:
     ```bash
     cdk synth
     ```

3. **Deploy the CloudFormation Template:**

   - Deploy your infrastructure:
     ```bash
     cdk deploy
     ```
   - If the deployment fails, follow the steps to create a role with the necessary permissions and redeploy.

#### Deployment Troubleshooting

- If deployment fails due to a missing role, create a JSON file (`zombine.json`), copy the role specifications, and create the role using AWS CLI.
- Verify the role creation and attach the `AdministratorAccess` policy.
- Retry the deployment with `cdk deploy`.

#### Final Steps

- Verify your S3 bucket is live by checking the AWS S3 console.
- Initialize a GitLab repository and push your project:

  ```bash
  git config --global user.name "Your Name"
  git config --global user.email "your.email@example.com"
  git init --initial-branch=main
  git remote add origin https://gitlab.com/yourusername/yourproject.git
  git add .
  git commit -m "Initial commit"
  git push --set-upstream origin main
  ```

Replace "Your Name" and "your.email@example.com" with your actual name and email. Adjust the GitLab repository URL to match your project's repository.

### Use of CodeWhisperer

For this project, AWS CodeWhisperer plays a crucial role in streamlining the development process by providing real-time coding recommendations and best practices. In developing the S3 bucket, I utilized specific prompts to instruct AWS CodeWhisperer, such as requesting the creation of an S3 bucket with key properties including versioning and encryption, alongside defining the necessary variables for the bucket's creation. 